---
date: 2017-02-13
title: Using VS Code as a git diff tool
subtitle: 
tags: 
---

# Using VS Code as a git diff tool

2017-02-13

As Visual Studio Code continues to evolve there are some command line switches that make it even more interesting to use.

By issuing `/path/to/vscode sourcefile destinationfile --diff` you can have VS Code display a visual file diff. On its own that is somewhat useful if you have to files in different locations you want to diff.

A more common use for a diff is with your source code management system, namely git.

You can setup an external diff tool in your .gitconfig with the following setting for VSCode for Windows.

```
[diff]
    tool = vscode
[difftool]
    prompt = false
[difftool "vscode"]
    cmd = \"C:\\Program Files (x86)\\Microsoft VS Code\\Code.exe\" \"$LOCAL\" \"$REMOTE\" --diff --wait
    trustExitCode = false
```

And to test it and get the following result create a throw away repo by way of

```
git init
echo foo > foo.txt
git add -A
git commit -am "1st commit"
echo foo1 > foo.txt
git difftool
```

and you should get something that looks something like 

![Silvrback blog image sb_float_center](img/vscode-diff.png)

[Once they have git merge UI ](https://github.com/Microsoft/vscode/issues/5770) support it would seem you could also use this as your external merge tool as well. 
