---
date: 2017-02-26
title: Making Hub Evergreen on Windows
subtitle: 
tags: 
---

# Making Hub Evergreen on Windows

2017-02-26

The [hub](https://github.com/github/hub) project from the folks over at GitHub is quite the nice piece of cli kit. I'm going to be going deeper on how I've been using in my projects in a following post. For this is just going to cover a step zero on getting it installed and keeping it current. I'd just prefer to have it installed in my machines and 'just work' so I wired up a little bit of PowerShell profile hooks to make sure I'm on the latest.

![Silvrback blog image sb_float_center](img/hub-wheel.png)

For my hub installs I want

- hub.exe in my path
- hub.exe installed off of my profile root at `$env:USERPROFILE` in my case `C:\Users\eric\hub-windows`

```powershell
# Capture username
$username = $env:USERNAME

# Check to see if Hub is in the env path
$hubPath = "C:\Users\$username\hub-windows\bin"
$hubInPath = ([Environment]::GetEnvironmentVariables("User").Path).Split(";") -contains $hubPath

# hub executable disk path
$hubPath = "C:\Users\$username\hub-windows\bin"
$hubOnDisk = Test-Path (Join-Path $hubPath "hub.exe")
$installHub = !$hubOnDisk

# Get the release information from the GH API for releases
$releasesResponse = Invoke-RestMethod -Uri "https://api.github.com/repos/github/hub/releases"
$hubRelease =  $releasesResponse[0].tag_name
$hubVersion = $hubRelease.Substring(1)
$uriBase = "https://github.com/github/hub/releases/download/$hubRelease/hub-windows-amd64-$hubVersion.zip"

if ($hubInPath -eq $true -and $hubOnDisk -eq $true) {
    $hubVersion = "v"+(hub version)[1].split(" ")[2]
    if($hubVersion -ne $hubRelease){
        Write-Host "hub in path but application needs updating. Current: $hubVersion, Latest: $hubRelease" -ForegroundColor Yellow
        $installHub = $true
    }
}

if ($hubInPath -eq $false) {
    Write-Host "Hub not in environment path. Updating user environment path variables" -ForegroundColor Yellow
    [Environment]::SetEnvironmentVariable("Path", ([Environment]::GetEnvironmentVariables("User").Path) + ";" + $hubPath, "User")
}

if ($hubOnDisk -eq $false -or $installHub -eq $true) {
    Write-Host "Downloading latest version from $uriBase" -ForegroundColor Yellow
    Invoke-WebRequest -Uri $uriBase -OutFile "C:\Users\$username\hub-windows.zip"
    Expand-Archive -Force -Path "C:\Users\$username\hub-windows.zip" -DestinationPath "C:\Users\$username\hub-windows"
    Remove-Item -Path "C:\Users\$username\hub-windows.zip"
}

Write-Host "hub installed and current" -ForegroundColor DarkGreen
```

by having this **just there** I can keep it current and get one with working with my github repos from the command line - Like a Good Developer Should™. I do similar things with [`posh-git`](https://github.com/dahlbyk/posh-git) where I always make sure I'm on the latest from master on that project.
