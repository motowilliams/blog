---
date: 2017-02-11
title: Building a Solution with dotnet cli
subtitle: 
tags: 
---

# Building a Solution with dotnet cli

2017-02-11

The latest version of the [dotnet cli](https://github.com/dotnet/cli) is offering some nice features for getting a .NET software solution started. This can enable scenarios where you want to start a project off with some guide rails on where you want your team to put things. [Instructions for installing are here](https://github.com/dotnet/templating/blob/master/README.md#how-to-build-and-run-the-latest)

The simple example to talk about it `dotnet new mvc` which will create a .NET Core MVC project for you in the current directory. You can create class libraries with `dotnet new classlib` and unit tests with `dotnet new xunit`. This is great for demos but now we have a handful of silos projects that really aren't connected in any meaningful way.

What we want to do is add some references between these projects so that we keeping things separated in a meaningful way. This is where the project reference switches some in. By using `dotnet add targetprojects.csproj reference sourceproject.csproj` we can wireup our project references.

So this is getting a little better, what about that odd looking solution file? There are commands for that as well. `dotnet new sln --name YourSolutionName` will create the solution and to add projects to the solution we issue `dotnet sln add sourceprojectpath.csproj` until we have all of them in the sln file.

If you wanted to create a solution to build the somewhat typical layout like 

![Silvrback blog image sb_float_center](img/IMG_0431.png)

I wrapped some of these commands up into a simple powershell script and ended up with this:

```powershell
Param(
    [string] [Parameter(Mandatory=$true)] $SolutionName,
    [ValidateSet('netcoreapp1.0','netcoreapp1.1')]
    [string] $framework = "netcoreapp1.1"
)

# Create source directory and change directory to it
New-Item -ItemType Directory -Path $SolutionName | Out-Null
Set-Location -Path $SolutionName

# Set some defaults names & paths
$webProject = "Web"
$modelsProject = "Models"
$servicesProject = "Services"
$servicesTestProject = "$servicesProject.Tests"

$webProjectPath = "$webProject\$webProject.csproj"
$modelsProjectPath = "$modelsProject\$modelsProject.csproj"
$servicesProjectPath = "$servicesProject\$servicesProject.csproj"
$servicesTestProjectPath = "$servicesTestProject\$servicesTestProject.csproj"

#Create all of our individual projects
dotnet new mvc --name $webProject --framework $framework 
dotnet new classlib --name $modelsProject --framework $framework
dotnet new classlib --name $servicesProject --framework $framework
dotnet new xunit --name $servicesTestProject --framework $framework

#Add all of our individual projects to the solution
dotnet new sln --name $SolutionName
dotnet sln add .\$webProjectPath
dotnet sln add .\$modelsProjectPath
dotnet sln add .\$servicesProjectPath
dotnet sln add .\$servicesTestProjectPath

# Add all the of the nessessary project references
dotnet add .\$webProjectPath reference .\$modelsProjectPath
dotnet add .\$webProjectPath reference .\$servicesProjectPath
dotnet add .\$servicesProjectPath reference .\$modelsProjectPath
dotnet add .\$servicesTestProjectPath reference .\$modelsProjectPath
dotnet add .\$servicesTestProjectPath reference .\$servicesProjectPath

dotnet restore
dotnet build ".\$SolutionName.sln"
dotnet test .\$servicesTestProjectPath
```

Here is this running in VS Code in the [integrated terminal](a-better-integrated-terminal-for-vs-code)
![Silvrback blog image sb_float_center](img/Vi2yWLH-30.gif)

The new cli features also support a template solution so that you can just have your destination result persisted in a well known location. This could be interesting to keep your project templates in a nuget feed, a GitHub repo or even just on the filesystem somewhere. That's coming [in my next post](building-a-solution-with-dotnet-cli-templates).
