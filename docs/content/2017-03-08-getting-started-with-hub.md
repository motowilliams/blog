---
date: 2017-03-08
title: Getting Started with Hub
subtitle: Issues and Pull-Requests and Browse oh my!
tags: 
---

# Getting Started with Hub
## Issues and Pull-Requests and Browse oh my!

2017-03-08

![Silvrback blog image sb_float](img/IMG_0492.PNG)[Earlier I setup a script to keep hub fresh](making-hub-evergreen-on-windows) and smooth with the latest release from GitHub. Reason being I wanted to get a walk-thru started on actually using it.

## hub auth

First thing that is going to happen when you try to use hub is an authentication sequence.

```
github.com username: motowilliams
github.com password for motowilliams (never stored):
two-factor authentication code: 8675309
```
this will add a `.config` directory in your user profile as well as create a [Personal Access Token](https://github.com/settings/tokens) in your GitHub profile with a name of  `hub for username@machinename`

## hub init & create

### hub init
`hub init` think `git init` which will initialize a git repository in the current directory. Add a readme.md file so that your repo has a good initial state. Doing this local work does not give your a corresponding repo in GitHub. However...

### hub create

Magically `hub create` will make the necessary API calls to give you a shinny new GitHub repo under your user account. It will also update your origin to point to that location.

Push your local repo to the new remote you just created, `git push`

If you want to create a repo in an organization you have access to it would be `hub create [organization/name]` or in you want a private repo `hub create [organization/name] -p`

## hub browse

If you would like to see your repo just issue `hub browse` and your default browser takes you there. Nice repo by the way even if you don't have much there.

## hub issue

Let's say you need to get some GitHub issues to track what your project. Since you don't have anything there and your TPS Reports will need an audit trail let's create some.

`hub issue create -m "Add super awesome feature to do the awesome"`

that could take a little bit so have your product owner create some in their favorite tool aka a spreadsheet and you can grab them

```powershell
$issues = @(
"Add feature that allows signups",
"Add feature that allows payments",
"Add feature that sends notifications"
)

$issues | Foreach-Object { hub issue create -m $_ }
```

`hub issue create` also allows you do assign to a user, milestone and label which is really nice when you get into a situation where you need to bulk upload issues to kick off a project.

A quick shortcut to get to your issues is `hub browse --issues`

## hub pull-request

Another common workflow that you'll encounter is doing work on a topic branch, getting it finalized and ready to merge into your main line. Hopefully you are leveraging [pull-requests](https://help.github.com/articles/creating-a-pull-request/) for this and these are easy enough to create in the GitHub portal but `hub pull-request -m "your pull request message`

Here is an example of putting all the pieces together.

![Silvrback blog image sb_float_center](img/hub-getting-started-01a.gif)
    
