---
date: 2018-01-25
title: Using VSCode as your git editor
subtitle: 
tags: 
---

# Using VSCode as your git editor

2018-01-25

I noticed that the latest revisions of the git installer was showing VSCode as an option to be the git editor. This means for features where you have to work with a commit message or a rebase todo you can use VS Code to do your task.

![Silvrback blog image sb_float_center](img/Git-Logo-1788C_medium.png)

To enable this all you have to do is  add a line under your `[core]` node so that you have the following

```text
[core]
	editor = code --wait
```

Then viola!

![Silvrback blog image ](img/git-and-code.gif)
