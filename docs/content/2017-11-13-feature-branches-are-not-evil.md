---
date: 2017-11-13
title: Feature branches are not evil
subtitle: 
tags: 
---

# Feature branches are not evil

2017-11-13

I seem to get a lot of source control, build and other CICD content in front of me during the course of a day. One of the reoccurring themes that comes up occasionally is against feature branching. The argument usually suggests the following approach.

> when doing feature branching your CI stands for "continuous isolation"

with a clever diagram looking something like

![Silvrback blog image sb_float_center](img/continous-isolation.png)

Most of the time the author is really against long living feature branches for which I can agree with. There are also a number of times where the author will seem to think that you won't run any of your CI automation when you have any flavor of a topic branch in flight. This, I certainly do not agree with. Similar to having a [scheduled trigger to keep your builds happy](automated-builds-avoid-a-time-bomb) I think that having your branch spec setup to build almost every branch you push is a good place to start or at least have some common agreements within your team so that you always build `master`,`develop`,`feature/*`, `hotfix/*`, `bugfix/*` and `release*/*`. By setting your CI up this way you get constant feedback to your developers on their work in progress from the build process and [possibility you'll be able to see how your topic branches will handle in your deployments](arm-templates-in-your-cicd-pipeline) as well.

### Remember the ABCs 
> Always Be Continuously integrating

The moral of the story is that you do not have to be isolated when you are working things in a branch. Every dot on the graph potentially gets a pipeline for it.

As for how you actually pull this off. With a little bit of git know how you can keep your topic branches current with what is going on in your mainline. Learn rebase. It keeps things clean.

![Silvrback blog image sb_float_center](img/Presentation1.gif)
