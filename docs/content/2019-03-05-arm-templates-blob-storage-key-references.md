---
date: 2019-03-05
title: ARM Templates - Blob Storage Key References
subtitle:
description: Tips for ARM Template authoring
tags:
---
{% if page and page.meta and page.meta.subtitle %}
{{ page.meta.subtitle }}
{% endif %}
{% if page and page.meta and page.meta.date %}
{{ page.meta.date }}
{% endif %}

When you are continuously deploying your infrastructure with Azure Resource Manager templates (ARM) I don't want to bother my release pipeline with creating blob storage, requesting the key and then adding that to other resources - at least when it can be avoided. With the `resourceId` reference I can get those keys from the `listKeys` function. Then I can have all my resources created and their configuration values referenced all from within my ARM deployment.

For referencing the keys you can use the following: `listKeys(resourceId('Microsoft.Storage/storageAccounts', variables('storageAccountName')), '2015-05-01-preview').key1` & `listKeys(resourceId('Microsoft.Storage/storageAccounts', variables('storageAccountName')), '2015-05-01-preview').key2`

You do need to make sure that whatever is going to be requesting these values has the proper resource dependencies configured so that the storage account is created before the key is requested.

```text
"dependsOn": [
    "[resourceId('Microsoft.Storage/storageAccounts', variables('storageAccountName'))]"
]
```

If there external process that ultimately need those keys you can could then add them to your outputs section of your template.

```text
 "outputs": {
    "storageAccountKey1": {
        "type": "string",
        "value": "[listKeys(resourceId('Microsoft.Storage/storageAccounts', variables('storageAccountName')), '2015-05-01-preview').key1]"
    },
    "storageAccountKey2": {
        "type": "string",
        "value": "[listKeys(resourceId('Microsoft.Storage/storageAccounts', variables('storageAccountName')), '2015-05-01-preview').key2]"
    }
}
```
