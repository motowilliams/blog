---
date: 2017-01-08
title: A Better Integrated Terminal for VS Code
subtitle: 
tags: 
---

# A Better Integrated Terminal for VS Code

2017-01-08

VS Code has been around for a while now and one of the nice features from this editor is the ability to start an integrated terminal shell within the application and in the context of your working directory. Out of the box Code ships with the default windows command prompt aka COMSPEC. This can be use for some tasks but having Powershell in that terminal is quite a bit more useful.

![sb_float](img/vscode-powershell_large.png "VS Code and Powershell")

To enable this customization you need to jump into your Code preferences and override a setting for the terminal you want to use. You can find these settings in the File > Preferences > User Settings or you can press F1 and type {user settings}  and press enter.

Here you will edit your user.settings json file so have a value of 

``` json
"terminal.external.windowsExec":
     "C:\\Windows\\sysnative\\WindowsPowerShell\\v1.0\\powershell.exe"
```

Now if you launch the integrated terminal you get a powershell session and as of VS Code version 1.8.1 is currently also loads in your Powershell profile. _There was a work around where needed you point to a wrapper script to get the profile to load._ Most of the time having your profile loaded is what you want especially if you are using [Posh-Git](https://github.com/dahlbyk/posh-git). There times when you want to have some added arguments passed to this command and to accomplish that you would add another item in your user.settings json file for additional shell arguments.

``` json
"terminal.integrated.shellArgs.windows": [
        "-NoLogo",
        "-NoProfile"
    ]
```
Another example configuration would be using git bash. Here you will pass some additional argument to make sure you land in your user directory.

``` json
"terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",
    "terminal.integrated.shellArgs.windows": ["--cd-to-home"]
```

    
