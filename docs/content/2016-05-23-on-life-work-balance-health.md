---
date: 2016-05-23
title: On Life Work Balance - Health
subtitle: 
tags: 
---

# On Life Work Balance - Health

2016-05-23

There is nothing more important than your health##
The reason I say this is because everything in your life can be derived from it. If you're not healthy you aren't happy. If you're not healthy you probably can't work to your full potential which in turn limits the resources available to you in forms on sustenance. If you can't eat you're not going to be very healthy. It's a vicious cycle.
##Where to start?##
Get up. Seriously just get up on your feet. Take a walk, even if you can't go very far just get outside where the Vitamin-D lives and walk around the block. If that is too far at first then walk around the half the block. Just move!
##What's next?##
Measure. Track what you're doing and when you're doing it. Nothing can build a good habit better than having a story of what you've done. You can use this to humble-brag and even guilt yourself into keeping it up.
##Get a routine##
I have three children and children need routines. I don't think that this something that we outgrow. Once you have a history of at least moving then you can see where you can add more items to your health routine.
##Take a day off##
When you start you're probably going to be have lots of motivation. This honeymoon phase will end and you're going to feel like taking a day off. This is fine and this is normal. The hard part is making sure that day or two off doesn't develop into a story and routine of its own. Habits, both good and bad, thrive on consistency.
