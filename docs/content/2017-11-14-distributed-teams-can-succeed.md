---
date: 2017-11-14
title: Distributed Teams Can Succeed
subtitle: 
tags: 
---

# Distributed Teams Can Succeed

2017-11-14

How is your company setup for distributed work?
The latest big name company bringing home their home reporting teams has made me finally want to attempt to document my thoughts on the topic. When it comes to teams working from home I don't see this is a zero sum game but more of a sliding scale. One side the tank is empty and working from home is just not an option and other side is a magic unicorn of a team who just excels at it.

![Silvrback blog image sb_float_center](img/guage.png)

_Disclaimer: I've only been doing it since [October 3, 2011](http://www.wolframalpha.com/input/?i=October+3,+2011) so I might have more things to figure out._

## No Remote Options
This is an obvious definition. You don't work from home - ever. You don't have web based email no VPN to get to back office. It is just not an option. I suppose there are IT shops still setup this way but I suspect they are far and few between.

## Remote on Demand / Remote Tolerant 
This is the place where you can occasionally work from home for a get stuff done at the house day or when you are a little under the weather and you don't want to get your team mates sick. _Thanks for thinking of us!!_

Here you can get to remote email, your IT department has setup a VPN so that if there are cases where you need to access resources that only live at HQ you can, technically, get to them.

> This was really only put in place so the admins could bounce a server but someone else got wind of it and they wanted in.

This scenario is better than nothing but there is enough friction that you wouldn't want to make a habit of it. You end up sending a bunch of emails to team mates just to get simple communications flows progressing. You use chat a little bit around the office and not having that muscle memory in your team shows when you try to use chat away from the office.

These are also the shops that might have a single or few workers who don't work in the office. There is a good chance they have a low quality of work life.

## Remote Friendly

Here is where most of the companies live that want to embrace distributed work. You can accomplish your tasks fairly seamlessly from home, a cafe or on the Bitterroot River where you have pretty good 4G coverage with little to no difference in work experience. These shops tend to leverage mostly SaaS services and they no longer have assets in the office that you need to connect to.

There are a few things that prevent them from progressing to the next level. These are mostly just friction points that your team will be annoyed with.

### Determining Presence
That green light next to their name in chat indicates they they are available for communication right? Maybe so, maybe not. I think the chat clients are doing a terrible job at this. It takes way too many steps to signal that someone is in a meeting or is deeply engaged in a task and would prefer to not be interrupted. Calendar integration should just work and when someone has their designated application in focus then chat should update a users status.

> Eric is in a meeting

or

> Eric is currently typing in his terminal

### Conversation Flow
This is mostly just an async communication problem but how often are you communicating with someone in a fairly tight back and forth and the other party just disappears from the conversation? What happened? Did an meteor strike their location?

### Is everyone remote?
I think that a company that has an actual office that has a significant amount of staff is going to have a hard time escaping from this level. This is not deliberate but the natural tenancy is just look up and start discussing something project related. This is tough to do but try to put everything into chat so the entire team and can see it, act on it and be able to search it.

## Remote First
This group is rare but they take all of the positives and really none of the negatives of the groups listed before. These rare beasts work exactly the same everywhere with little to no friction. These teams have a habit of keeping project related conversations in chat and can easily take a conversation quickly to a audio and video medium to keep things moving forward. They do this without having the mentally pause to see if something is web cam worthy. They also do this regardless of having team members co-located.

> I was a member of a team that had this ability. One of the key components was having really good audio video gear. We also had VOIP desk phones with all the team members accessible via speed dial. That hop from keyboard to voice was easy. The new integrated features in chat clients are a good step in the right direction but the voice quality and latency need to be improved.
