---
date: 2016-11-19
title: Powershell Networking Utilities
subtitle: Your ping, tracert, nslookup, ipconfig et al ...
tags: 
---

# Powershell Networking Utilities
## Your ping, tracert, nslookup, ipconfig et al ...

2016-11-19

Feeling the effects of Stockholm Syndrome while being captured by powershell I've been trying to search out the native powershell way of doing things before I throw my hands up and 'do it the old' way. I've been working more and more Azure lately and that typically means moving websites and applications from on-premise to the cloud so networking and DNS and lions and tigers etc etc.

![Silvrback blog image](img/tools_large.jpg)

Here is what I've been using for some of these verification and troubleshooting tasks.

---

### Ping

Let's start with the very first thing we all learned when troubleshooting our dial-up, cable modem, DSL and WiFi connections. [Ping!](https://en.wikipedia.org/wiki/Ping_(networking_utility)) is a simple network utility and checks to see if your current machine can reach a destination machine. The Powershell version of ping is called Test-NetConnection aliased as TNC.

```Powershell
Test-NetConnection -ComputerName example.com

ComputerName           : example.com
RemoteAddress          : 93.184.216.34
InterfaceAlias         : Ethernet 2
SourceAddress          : 192.168.42.20
PingSucceeded          : True
PingReplyDetails (RTT) : 25 ms
```
This seems overly verbose but you can get to some more details in a structured way.

```Powershell
» $result = Test-NetConnection -ComputerName example.com
» $result.RemoteAddress
Address            : 584628317
AddressFamily      : InterNetwork
ScopeId            :
IsIPv6Multicast    : False
IsIPv6LinkLocal    : False
IsIPv6SiteLocal    : False
IsIPv6Teredo       : False
IsIPv4MappedToIPv6 : False
IPAddressToString  : 93.184.216.34
» $result.RemoteAddress.IPAddressToString
93.184.216.34
```
This could make it worth using and Powershell cmdlets do have various shorthand methods so I might be hurting my argument by using the using the chatty way of using it.

You do get something interesting when you start looking at other parameters and switches of ping, I mean, Test-NetConnection

```Powershell
(Test-NetConnection -ComputerName example.com -InformationLevel Quiet) -eq $false
```
Now that opens up some opportunities - if example.com isn't reachable then do set a condition for something else.

---

### Traceroute

Next up is [traceroute](https://en.wikipedia.org/wiki/Traceroute). This utiltiy will show you information about all the nodes or hops along to way to getting to your destination.

In Powershell it is Trace-Route. Hold on. I'm being told by the red text in my terminal that *The term 'Trace-Route' is not recognized as the name of a cmdlet, function, script file, or operable program.* Fine then let's go back to the last thing that worked here.

```Powershell
 Test-NetConnection -ComputerName example.com -TraceRoute
ComputerName           : example.com
RemoteAddress          : 93.184.216.34
InterfaceAlias         : Ethernet 2
SourceAddress          : 192.168.42.20
PingSucceeded          : True
PingReplyDetails (RTT) : 25 ms
TraceRoute             : 192.168.42.1
                         0.0.0.0
                         69.144.234.221
                         72.175.111.40
                         72.175.110.40
                         72.175.111.156
                         206.81.80.152
                         192.16.54.88
                         93.184.216.34
```
Here you reuse the Test-NetConnection with a -TraceRoute flag. This is showing you the hops from my home network to the destination.

---

### NSLookup

[nslookup](https://en.wikipedia.org/wiki/Nslookup) gives us information about [DNS](https://en.wikipedia.org/wiki/Domain_Name_System).

```Powershell
» Resolve-DnsName -Name example.com

Name                                           Type   TTL   Section    IPAddress
----                                           ----   ---   -------    ---------
example.com                                    AAAA   2359  Answer     2606:2800:220:1:248:1893:25c8:1946
example.com                                    A      2359  Answer     93.184.216.34
```

Here we can see the A records for the example.com. The CNAME query is handy as well.

``` Powershell
» Resolve-DnsName -Name www.motowilliams.com -Type CNAME

Name                           Type   TTL   Section    NameHost
----                           ----   ---   -------    --------
www.motowilliams.com           CNAME  60    Answer     silvrback.herokuapp.com
```

Which shows where this very site is actually not hosted in my parents basement. Thanks [silvrback!](https://www.silvrback.com/)

---

### IPConfig

[ipconfig](https://en.wikipedia.org/wiki/Ipconfig) allows you to see your local TC/IP and DNS settings.

```Powershell
» Get-NetIPConfiguration


InterfaceAlias       : VirtualBox Host-Only Network #3
InterfaceIndex       : 10
InterfaceDescription : VirtualBox Host-Only Ethernet Adapter #3
IPv4Address          : 192.168.56.1
IPv6DefaultGateway   :
IPv4DefaultGateway   :
DNSServer            : fec0:0:0:ffff::1
                       fec0:0:0:ffff::2
                       fec0:0:0:ffff::3

InterfaceAlias       : VMware Network Adapter VMnet1
InterfaceIndex       : 2
InterfaceDescription : VMware Virtual Ethernet Adapter for VMnet1
IPv4Address          : 192.168.101.1
IPv6DefaultGateway   :
IPv4DefaultGateway   :
DNSServer            : fec0:0:0:ffff::1
                       fec0:0:0:ffff::2
                       fec0:0:0:ffff::3

InterfaceAlias       : VMware Network Adapter VMnet8
InterfaceIndex       : 27
InterfaceDescription : VMware Virtual Ethernet Adapter for VMnet8
IPv4Address          : 192.168.58.1
IPv6DefaultGateway   :
IPv4DefaultGateway   :
DNSServer            : fec0:0:0:ffff::1
                       fec0:0:0:ffff::2
                       fec0:0:0:ffff::3

InterfaceAlias       : Ethernet 2
InterfaceIndex       : 8
InterfaceDescription : Dell GigabitEthernet
NetProfile.Name      : MotoWiFi
IPv4Address          : 192.168.42.20
IPv6DefaultGateway   :
IPv4DefaultGateway   : 192.168.42.1
DNSServer            : 192.168.42.1

InterfaceAlias       : Wi-Fi
InterfaceIndex       : 11
InterfaceDescription : Intel(R) Dual Band Wireless-AC 8260
NetAdapter.Status    : Disconnected
```

As you can see here I have Vbox and Vmware on my machine, I'm currently docked so my Wi-Fi is disconnected.

---

Another aspect id ipconfig id refreshing your machines DNS settings. So in keeping that we'll just do 

```Powershell
» Get-NetIPConfiguration -FlushDNS
Get-NetIPConfiguration : A parameter cannot be found that matches parameter name 'FlushDNS'.
```

Come on! Ok let's try 

``` Powershell
Clear-DnsClientCache
Get-DnsClientCache
```
That will clear and refresh or flush our DNS so we can get the freshly minted powershell.example.com

``` Powershell
Resolve-DnsName -Name powershell.example.com -Type CNAME
Resolve-DnsName : powershell.example.com : DNS name does not exist
```
Or not.

---

Well that's a portion of how I've trying to get into the Powershell mindset of things. There is still a lot of searching around for me because I contiune to get bit by the [Principle of Least Astonishment](https://en.wikipedia.org/wiki/Principle_of_least_astonishment)
