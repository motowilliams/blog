---
date: 2016-10-26
title: TeamCity Messages with Psake
subtitle: Without Psake Contrib
tags: 
---

# TeamCity Messages with Psake
## Without Psake Contrib

2016-10-26

Controlling the Wall of Text
![Silvrback image](img/psake%2Btc_large.png)

Depending on what you have in your tasks as far as verboseness or even how many tasks your given build may have it is nice to have some structure to the build output so that the [next person](http://wiki.c2.com/?CodeForTheMaintainer) can retain some sanity when looking at the build logs. It will put the logs for each task into a collapsible tree view. Tasks that contain lost of entries like npm and nuget package restores can be easily rolled up and looked past.

![Silvrback image](img/Capture_large.PNG)

## Wrap Your Tasks
Psake makes this fairly easy with the use of the **TaskSetup** and **TaskTearDown** functions for respective hooks for execute code before and after you task runs. The documentation on other tasks can be found here, http://psake.readthedocs.io/en/latest/structure-of-a-psake-build-script/

## Where Am I?
If you pair that with a quick environment variable check for the presence of a TeamCity build agent you can conditionally write TeamCity specific messages to the build log.

``` Powershell
TaskSetup{
    if($env:TEAMCITY_VERSION){
        Write-Output "##teamcity[blockOpened name='$taskName']"
    }
}

TaskTearDown{
    if($env:TEAMCITY_VERSION){
        Write-Output "##teamcity[blockClosed name='$taskName']"
    }
}
```
