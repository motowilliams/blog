---
date: 2018-01-22
title: ARM Templates - Reading Output Variables
subtitle: 
tags: 
---

# ARM Templates - Reading Output Variables

2018-01-22

When using ARM templates for your deployments you will find your build or your release pipeline in a situation where you need to get some of those generated values from your template or some references from the resources that ARM created in Azure for you.

![Silvrback blog image sb_float_center](img/arm-output_large.png)

I have a couple of PowerShell functions that end up getting leveraged in my VSTS Build and Release pipelines. These are executed right after my Azure Resource Group Deployment VSTS task.

First is **Get-AzureRMDeploymentOutputVariables** which uses the `Get-AzureRmResourceGroupDeployment` Azure PowerShell command to get a list of the deployments, sorted by the latest then grabbing the last deployment. Then I just convert the outputs to a `HashTable` and return that to the PowerShell pipeline.

```powershell
function Get-AzureRMDeploymentOutputVariables {
    [CmdletBinding()]
    param (
        [string]$ResourceGroupName
    )

    process {

        $azureRmResourceGroupDeployment = Get-AzureRmResourceGroupDeployment -ResourceGroupName "$ResourceGroupName" | `
            Sort-Object Timestamp -Descending | `
            Select-Object -First 1

        $variables = @{}
        $azureRmResourceGroupDeployment.Outputs.GetEnumerator() | ForEach-Object {
            $variables.Add($_.key,$_.value.Value)
        }

        return $variables
    }
}

```

> Warning: this _might_ be a little brittle for resource groups that could potentially have multiple deployments going at once.

Second up is the **Set-VstsPipelineVariables**. This script takes a pipelined hashtable and uses the [VSTS output format](https://github.com/Microsoft/vsts-tasks/blob/master/docs/authoring/commands.md) to assign those are build or release environment variables.

```powershell
function Set-VstsPipelineVariables {
    [CmdletBinding()]
    param (
        [parameter(ValueFromPipeline)][HashTable]$Variables
    )

    process {

        $Variables.GetEnumerator() | ForEach-Object {
            $variableName = $_.Name
            $variableValue = $_.Value

            Write-Verbose "Creating new variable: $variableName $variableValue"
            Write-Host "##vso[task.setvariable variable=$variableName;]$variableValue"
          
        }
    }
}

```

Then these are referenced in my Build and Release tasks such as `Get-AzureRMDeploymentOutputVariables | Set-VstsPipelineVariables`

Then you can expecting something similar in your VSTS task logs.

```text
##[section]Starting: Azure PowerShell: Export ARM Output Variables ==============================================================================
Task         : Azure PowerShell
Description  : Run a PowerShell script within an Azure environment
Version      : 2.0.3
Author       : Microsoft Corporation
Help         : [More Information](https://go.microsoft.com/fwlink/?LinkID=613749)
==============================================================================
##[command]Import-Module -Name C:\Program Files\WindowsPowerShell\Modules\AzureRM\5.1.1\AzureRM.psd1 -Global
##[command]Import-Module -Name C:\Program Files\WindowsPowerShell\Modules\AzureRM.Profile\4.1.1\AzureRM.Profile.psm1 -Global
##[command]Add-AzureRMAccount -ServicePrincipal -Tenant ******** -Credential System.Management.Automation.PSCredential -Environment AzureCloud
##[command]Select-AzureRMSubscription -SubscriptionId get-your-own-azure-subscription -TenantId ********
##[command]& 'C:\VSTSAgent\_work\_temp\a5902e51-c5eb-4ccb-b8b9-9863e6006fa5.ps1' 
VERBOSE: Creating new variable: storageAccountKey1SecretUri 
VERBOSE: https://somevaulthost.vault.azure.net/secrets/storageAccountKey1
VERBOSE: Creating new variable: storageAccountKey2SecretUri 
VERBOSE: https://somevaulthost.vault.azure.net/secrets/storageAccountKey2
VERBOSE: Creating new variable: storageAccountName yourstorageaccountname
VERBOSE: Creating new variable: storageAccountKey1 
VERBOSE: +MXyp2wDC**********************************p7w1F6m+ibVog==
VERBOSE: Creating new variable: storageAccountKey2 
VERBOSE: UGx5JZa8**********************************qAKtOSf7yWHHA==
VERBOSE: Creating new variable: resourceGroupUniqueString u2dayhqjuii2g
VERBOSE: Creating new variable: webAppName yourwebappname
VERBOSE: Creating new variable: hostNameBindings_Internal yourwebappname.azurewebsites.net
##[section]Finishing: Azure PowerShell: Export ARM Output Variables
```
    
