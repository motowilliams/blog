---
date: 2017-02-21
title: Building a Solution with dotnet cli nuget templates
subtitle: 
tags: 
---

# Building a Solution with dotnet cli nuget templates

2017-02-21

If we move past the previous post thing I think we end up on a little 's' series for which I walked though some uses of `dotnet new`:

- [Building a Solution with dotnet cli](building-a-solution-with-dotnet-cli)
- [Building a Solution with dotnet cli templates](building-a-solution-with-dotnet-cli-templates)

With the last post ending up having a reusable solution template but it was living on the file system. This might be fine if you team has access to a local share or an otherwise easily accessed location. If that is the case leave it at that and move on to creating awesome .NET Core apps. You might have a case where you want to publish your templates to something more accessible to your remote team of even the public community at large. With that we're going to continue on and publish our fresh template to a nuget feed.

![Silvrback blog image sb_float_center](img/template-package.png)

There are no a number of options for hosting nuget packages. Nuget.org, Myget or even your local file system. Yes, your local file system can be used as a nuget source. This has been around for a while but as of nuget 3.3 there have been improvements on how those nuget packages are stored.

Once you have your template in a state where you want to publish it you have create a nuspec file. These are pretty simple and there are plenty of docs and specification information on those.

```xml
<?xml version="1.0" encoding="utf-8"?>
<package xmlns="http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd">
  <metadata>
    <id>myTemplate</id>
    <version>$version$</version>
    <authors>Some Author</authors>
    <description>Simple nuget package for dotnet new templating</description>
    <language>en-US</language>
    <projectUrl>https://example.com</projectUrl>
    <licenseUrl>https://example.com</licenseUrl>
    <packageTypes>
      <packageType name="Template" />
    </packageTypes>
  </metadata>
</package>
```

Assuming nuget is in your path calling `nuget pack path\to\your\specfile` will create a nupkg file that you then can add to your feed. After you're done publishing, to a file system or a service then you can just install that template with `dotnet new -i path\to\your\feed` or `dotnet new -i https:\\your.feed.uri` and it will then be available to your template engine.

![Silvrback blog image sb_float_center](img/dotnetcli-demo-nuget-01-compressed.gif)

which you can see when you call `dotnet new -i -all` which displays all your installed templates.
