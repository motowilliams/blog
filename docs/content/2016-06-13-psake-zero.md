---
date: 2016-06-13
title: Psake Zero
subtitle: Interation / Sprint Zero
tags: 
---

# Psake Zero
## Interation / Sprint Zero

2016-06-13

The Zero

Most projects start with a fresh directory on someone's machine at some undisclosed location. Then what? Basically this is the last point where there seems to be any agreement but I've been using basically the same setup for many projects. I've had these starter concepts spawn other projects from other developers so there seems to be at least a +1 besides me that likes this approach. It is fairly developer friendly and [Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration) server friendly with at least [JetBrains TeamCity](https://www.jetbrains.com/teamcity/) and [Atlassian Bamboo](https://www.atlassian.com/software/bamboo). This starting point has generally been called [Iteration](http://www.jrothman.com/articles/2012/10/how-to-use-iteration-zero-or-not/) or [Sprint](https://www.infoq.com/news/2008/09/sprint_zero) Zero.

## The Rundown

The folder structure is sane with the general rule suggestion being trying to keep the root directory relatively clean and lean.

```
$\
  artifacts\
  build\
  packages\
  src\
  tests\
  .gitattributes
  .gitignore
  readme.md
  run.ps1
```

**Artifacts** - these are the output artifacts that are leveraged for other processes down the dependency  build chain. The substructure of this is fluid based on what the project type is. An example here might be packages, services or web.

**Build** - these are the build scripts and script modules that I keep in source. Think of this has a read-only directory.

**Packages** - I keep the packages up at the root so that my build tools and source code can share items that are restored from a central location.

**Src** - the source code root. This makes it easy to type ```msbuild src\*.sln {tab}``` to get a quick native build going

**Tests** - at some point there will be a command that will pull all the test assemblies and their dependencies here so that the command line test runners can do some work.

The dot files and Readme are what you'd like but GitHub has a [nice collection of ignore files](https://github.com/github/gitignore) as a starting point

**run.ps1** - is what I rename ```psake.ps1``` too. From here I can have developers keep their [posh-git](https://github.com/dahlbyk/posh-git) prompt parked at the repository root. I do make a couple of changes to the parameter order of the bootstrap script.

## Build Tool

[Psake](https://github.com/psake/psake) is a plain and simple a task runner that has some added features that also happen to make it a pretty good build tool. With that I want the developers to be able to fire off commands. I also want the CI server to execute the command is almost the identical way. When your developer machines and your CI agent are close to mirrors as possible you will waste less time messing around with build issues that have the famous *work's on my machine*.

## Changes from Psake Defaults

Once you have the Psake modules and bootstrap script installed you have a usable method to load the module without futzing around with profile mess but defaults need some help. I will push the ```taskList``` property to the first position and I'll set the location for the default.ps1 build script to be in ```build``` directory. Again I rename ```psake.ps1``` to ```run.ps1``` but that's just *taste*. If I use some tools that I have behind a package restore command I typically add those to the bootstrap script as well as going to the internet to fetch the package tools themselves - nuget, node, gulp tools etc.

## Executing Tasks

```r{tab}{tab}{enter}``` (two tabs to roll back the readme) lands on ```.\run.ps1``` will run the default build and doesn't seem to have enough keystroke churn to prevent developers from running it before they push to the remote. The default Psake task is usually setup to run the compile and the fast set of tests. Usually the devs who are aware that they are changing databases or infrastructure will run those tests as well, something like ```.\run.ps1 integration-tests```. Once things are pushed then the build server will typically call a CI task or a combination of comma separated tasks to constitute a CI build with the addition of passing a properties hash to override certain defaults we have defined in our build script. This might look something like ```.\run.ps1 -taskList CI -properties @{"octopublish"=$true;"version"="some version info"}```
