---
date: 2016-12-02
title: What, that tech still works?
subtitle: jQuery and string concatenation
tags: 
---

# What, that tech still works?
## jQuery and string concatenation

2016-12-02

There are times when you just need to add something to the DOM. Sometimes you just need to make a quick call to an HTTP endpoint and get some data to display. There are also times when you don't want to spin up a 42 step frontend build pipeline just to calculate the airspeed velocity of an unladen swallow.
![Silvrback blog image](img/IMG_0406_large.png)
I was looking at a way to add some data from GitHub to my little side project [psakezero.com](http://psakezero.com) to show that the project has some life by way of the issues list. It turns out that the nice folks there have an issues API that can take a query string filter https://api.github.com/repos/psakezero/script/issues?state=open that will show the open issues for a given repo.

I dropped a little snippet of javascript at the end of the page and my problem was solved. I'm not going to show you this but I started with this from [stackoverflow](http://stackoverflow.com/questions/26603273/possible-to-embed-github-list-of-issues-with-specific-tag-on-website/26608674#26608674). It isn't something to be proud of but that isn't really the point either. In this case I needed the simplest thing that could possibly work. It is done and I'm now writing this post(mortem).
![Silvrback blog image](img/IMG_0407_large.png)
We tend to over complicate things for, honestly, no good reason. Sometimes the golden oldies are exactly what we need. In this case the currency was just my own time but there are times when the solutions we deliver are being paid for by our clients. That Access database they have been running their business on for years just needs to be converted to a web page with a more reliable database behind it.
