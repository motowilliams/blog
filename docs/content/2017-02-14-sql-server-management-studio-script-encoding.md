---
date: 2017-02-14
title: Sql Server Management Studio Script Encoding
subtitle: Why are my diffs wrecked?
tags: 
---

# Sql Server Management Studio Script Encoding
## Why are my diffs wrecked?

2017-02-14

When you install Sql Server Management Studio (SSMS) 2016 and have it generate script files for you the encoding is set in such a way that git will show them as binary files and your diffs will be useless.

![Silvrback blog image sb_float_center](img/terminal.png)

To fix this open SSMS and go to the following menus:
Tools » Options » Environments » International Settings. From there changed the Language from "English" to "Same as Microsoft Windows" this setting sets SSMS to save your script files as UTF-8 after you restart the application.
