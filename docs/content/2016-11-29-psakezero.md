---
date: 2016-11-29
title: PsakeZero
subtitle: An Implementation
tags: 
---

# PsakeZero
## An Implementation

2016-11-29

> Complaining about a problem without proposing a solution is called whining-Theodore Roosevelt

![Silvrback blog image](img/IMG_0403_large.PNG)


It's a highly paraphrased quote but Teddy is one of my favorite presidents so we'll just pretend he said that during an expedition and move on. His mustache game was second to none.

New projects, how do you set them up? It can be a range from starting with a empty directory and a coin toss to see what the team comes up with. One the other end of the spectrum you can go all the way to a reference application that gets cloned and your team pulls out the construction tools to shape it to what fits best for that particular team for that particular project during a particular planetary alignment. I like a variation to this approach https://gist.github.com/davidfowl/ed7564297c61fe9ab814 possibliy because I've been doing it this way for many project across many employeers across many hunting seasons (I can't wait for archery season to start again).

Like anyone using computers, repetitive tasks beg me to automate them so with that I took a swing at some npn init / yeomen style powershell scripting to codify my opinions on this.

![sb_float](img/psake-zero_large.png "PsakeZero")

I call it [PsakeZero](https://psakezero.com). As in the zero'th step in getting started on a project but also because I have a [post titled PsakeZero](/psake-zero) on it. There is an added benefit that it is easy for me to remember and I can type in a terminal without having to open a browser and search out a gist, repo or some other link. The site has a little touch of content-negotiation to either serve up the site or serve up the script. It relies on the current state of Powershell's Invoke-WebRequest not sending an Accept header where a browser will at least request html. A little brittle but it currently works.

All PsakeZero does is prompt you for a handful of questions like what you want your source directory called, if you want to use nuget package restore , if you have database migrations and unit tests - from there it creates an empty tree. It will also drop in some git dot files and get a build script started for you based somewhat on your provided answers. Put it all behind a bootstrapper that I've been carrying around for years and that's it.

It is after hours work and some Thanksgiving downtime so it's very green. It's all on GitHub and open an issue if you'd interested.
