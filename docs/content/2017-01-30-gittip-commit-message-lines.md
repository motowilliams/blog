---
date: 2017-01-30
title: GitTip - Commit Message Lines
subtitle: 
tags: 
---

# GitTip - Commit Message Lines

2017-01-30

![Silvrback blog image sb_float](img/git-tip_sm.png)The other day I stumbled upon a neat little trick with the git commit message. You would normally git your commit flow starting with just a ```git commit``` command. This will start your default editor such as vim or another simple text editor. Another little shortcut for this is the ```-m``` switch. You can commit with a message with a ```git commit -m "This solves everything"``` and that will result in 

```
commit 105cb4f21f2445f5e063b69158db45546a522ff0
Author: Eric Williams <ericwilliams@example.com>
Date:   Mon Jan 30 12:00:00 2017 -0700

    This solves everything
```

No magic here but maybe that will save you a few strokes of the keyboard but moving on. Now if you look up some commit message formats http://chris.beams.io/posts/git-commit you might find suggestions around a subject line and message detail. One of the few format suggestions that I try to keep to and I see broken frequently is the around this. Keeping the _subject_ line to 50 characters or less, adding a blank line and then adding your commit detailed description will generally keep most of the displays happy when you are viewing commit log and messages.

Here is what happens in github when you let your first line go long ```git commit -am "Lorem ipsum dolor sit amet, consectetur cras amet Lorem ipsum dolor sit amet, consectetur cras amet"```

![Silvrback blog image sb_float_center](img/Untitled.png)

When you want to get the commit message format per these suggestions from the command line you can just add an additional ```-m``` switch.

So take the command ```git commit -m "Lorem ipsum dolor sit amet, consectetur cras amet" -m "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non faucibus mauris. Pellentesque pretium euismod lectus, ac ultricies elit faucibus vitae. Praesent pellentesque mi quis ipsum sollicitudin, non posuere mi tempus."```

```
commit de07ea03993553b4bcad5e909d46c7294c93709d
Author: Eric Williams <ericwilliams@example.com>
Date:   Mon Jan 30 12:00:00 2017 -0700

    Lorem ipsum dolor sit amet, consectetur cras amet

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce non faucibus mauris. Pellentesque pretium euismod lectus, ac ultricies elit faucibus vitae. Praesent pellentesque mi quis ipsum sollicitudin, non posuere mi tempus.
```

Which will get rendered in GitHub as 

![Silvrback blog image sb_float_center](img/Untitled2.png)
    
