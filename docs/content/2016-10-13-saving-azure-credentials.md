---
date: 2016-10-13
title: Saving Azure Credentials
subtitle: When using Powershell
tags: 
---

# Saving Azure Credentials
## When using Powershell

2016-10-13

Rinse / Repeat
![sb_float](img/PowerShell_5.0_icon_large.png "Powershell")When working with Azure Powershell you have to authenticate your cmdlet calls in order to actually get any work done.

This is pretty simple with [**Add-AzureRmAccount**](https://msdn.microsoft.com/en-us/library/mt619267.aspx) but having to do that for every script run gets tiresome fairly quick. To solve that you can save your credentials to a json file for a period of time with [**Save-AzureRmProfile**](https://msdn.microsoft.com/en-us/library/mt619249.aspx) and then you can load it with [**Select-AzureRmProfile**](https://msdn.microsoft.com/en-us/library/mt619310.aspx) 

## Automate the Automation

To wrap this I've been using this snippet:

``` Powershell
$profileFileName = "AzureRmProfile.json"
$ProfileFilePath = (Join-Path $env:Temp $profileFileName)

# Sign In / Save Profile
if((Test-Path $profileFilePath) -eq $false) {
    Write-Host "Logging in..." -ForegroundColor Yellow
    Login-AzureRmAccount
    Write-Host "Saving profile" -ForegroundColor Green
    Save-AzureRmProfile -Path $profileFilePath
}
Select-AzureRmProfile -Path $profileFilePath | Out-Null
```
 A few things to note. This *sample* is storing the profile output to a temp location. You'll probably want to get this into a [Powershell Module](https://technet.microsoft.com/en-us/library/dd901839(v=vs.85).aspx) where you can handle flows where the file name get overwritten or what happens when the file expires.
