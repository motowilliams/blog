---
date: 2016-05-23
title: On Life Work Balance - Life
subtitle: 
tags: 
---

# On Life Work Balance - Life

2016-05-23

Another aspect of this balancing act is your life. This is what you are about. What makes you tick? Keeping this portion of your balance in check, I feel, is closely related to the health angle we talked about. I think that there is a trap that especially software developers can fall into where their hobby is also software development.
I think this is great if one can have their hobby or side projects be different from the daily job but I've seen too many times where they are the same and the person just has a strong burn out.
Myself, it's the outdoors. If I don't get to shoot my bow at least a few times a week or even every day I get cranky. Getting regular exercise is no long even an option anymore and frankly isn't even up for debate - you have to do it.
I like to build things and I've told countless people that if it wasn't building software I would be building something else in some other medium.
