variable "cloudflare_email" {}
variable "cloudflare_api_key" {}
variable "cloudflare_zone_id" {}
variable "a_record_name" {}
variable "a_record_value" {}
variable "cname_record_name" {}
variable "cname_record_value" {}
variable "account_id" {}
