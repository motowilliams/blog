[CmdletBinding()]
param (
    [switch]$Save
)

[xml]$blog = Get-Content $PSScriptRoot\blog.xml
$contentDirectory = Join-Path -path $PSScriptRoot -ChildPath docs -additionalchildpath content
# $nav = @{}

$i = 0

$blog.blog.post | ForEach-Object {
    $i++
    # Write-Verbose "converting $($_.published_at)"
    [datetime]$date = $_.published_at
    [string]$post_date = $date.ToString("yyyy-MM-dd")
    $titleSlug = ($_.title)
    $titleSlug = $titleSlug -replace "\!|\.|\'|\:|\-|\,|\?", " "
    $titleSlug = $titleSlug.Trim()
    $titleSlug = $titleSlug -replace "\s{2,}", " "
    $titleSlug = $titleSlug.Trim()
    $titleSlug = $titleSlug -replace "\s", "-"
    $titleSlug = $titleSlug.Trim()

    # $directory = Join-Path -Path $contentDirectory -ChildPath "posts" -additionalchildpath $titleSlug
    $fileName = ("$titleSlug.md").ToLower()
    $filePath = Join-Path -Path $contentDirectory -ChildPath "$post_date-$fileName"
    Write-Host -ForegroundColor Green "$($_.published_at) - $fileName"

    if ($Save) {
        #New-Item -force -ItemType Directory -Path $directory | Out-Null
        Set-Content -Force -Path $filePath -Value $null
        Add-Content -Path $filePath -Value "---"
        Add-Content -Path $filePath -Value "date: $($post_date)"
        Add-Content -Path $filePath -Value "title: $($_.title)"
        Add-Content -Path $filePath -Value "subtitle: $($_.subtitle)"
        Add-Content -Path $filePath -Value "tags: "
        Add-Content -Path $filePath -Value "---"
        Add-Content -Path $filePath -Value ""
        Add-Content -Path $filePath -Value "# $($_.title)"
        if ($_.subtitle) {
            Add-Content -Path $filePath -Value "## $($_.subtitle)"
        }
        Add-Content -Path $filePath -Value ""
        Add-Content -Path $filePath -Value "$($post_date)"
        Add-Content -Path $filePath -Value ""
        Add-Content -Path $filePath -Value $_.body
    }

    # $nav.Add("$post_date-$i", "    - $fileName")
}

# $navFile = Join-Path -path $contentDirectory -ChildPath ".pages"
# Set-Content -Force -Path $navFile -Value "nav:"
# $nav.GetEnumerator() | Sort-Object -Descending { $_.Key } | ForEach-Object {
#     Write-Host "Adding $($_.key) - $($_.value)"
#     Add-Content -Force -Path $navFile -Value $_.value
# }