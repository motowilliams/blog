#!/bin/bash

INDEX="index.TMP"

echo "# Posts" > $INDEX

for f in `ls docs/content/*.md | sort -gr | grep -v index.md`
do
  dos2unix -q $f
  echo "" | tee -a $INDEX
  TITLE=$(grep '^title: ' $f       | cut -c8- | tr -d '\n')
  DATE=$(grep '^date: ' $f        | cut -c7- | tr -d '\n')
  DESCRIPTION=$(grep '^description: ' $f | cut -c14- | tr -d '\n')
  LINK=$(echo $f | cut -c14- | sed -E s/\.md$//g)

  echo '<div class="mdc-card mdc-card--outlined my-card-content">' | tee -a $INDEX
  echo "  <a href=\"$LINK\">" | tee -a $INDEX
  echo "  <b>$TITLE</b>" | tee -a $INDEX
  [ ! -z "$DESCRIPTION" ] && echo "  <p>$DESCRIPTION</p>" | tee -a $INDEX
  [ -z "$DESCRIPTION" ] && echo "  <br/>" | tee -a $INDEX
  echo "  <small>$DATE</small>" | tee -a $INDEX
  echo '</a></div>' | tee -a $INDEX

# echo "!!! tldr \"$TITLE\"" | tee -a $INDEX
# echo "    [$TITLE]($LINK) dolor sit amet, consectetur adipiscing elit. Nulla et euismod" | tee -a $INDEX
# echo "    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor" | tee -a $INDEX
# echo "    massa, nec semper lorem quam in massa." | tee -a $INDEX

done

mv $INDEX "docs/content/index.md"
